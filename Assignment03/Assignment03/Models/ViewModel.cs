﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment03.Models
{
    public class ViewModel
    {
        public String Search { get; set; }
        public IEnumerable<Assignment03.Employee> Employees { get; set; }
    }
}